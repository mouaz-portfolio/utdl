﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
using Utdl.Dao;

namespace Utdl.View {
    public partial class FcourseLapins:Form {
        private int idCourse;
        
        public FcourseLapins() {
            InitializeComponent();
            btnAdd.Click += this.btnAdd_Click;
            btnEdit.Click += this.btnEdit_Click;
            btnDelete.Click += this.btnDelete_Click;
            btnSave.Click += this.btnSave_Click;
            this.loadCourse(new DaoCourse().GetAll());
            cbCourse.SelectedIndexChanged += CbCourse_SelectedIndexChanged;
        }

        private void CbCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            int position = cbCourse.SelectedIndex;
            int idCourse = ((Course)cbCourse.Items[position]).Id;
            this.load(new DaoLapin().GetAll(idCourse));
            this.idCourse = idCourse;
            
        }

        private void btnSave_Click(object sender,System.EventArgs e) {
            List<Lapin> lapins = new List<Lapin>();
            foreach (object o in lbLesLapins.Items)
            {
                lapins.Add((Lapin)o);
            }
            new DaoLapin().SaveChanges(lapins);
            this.load(lapins);
        }

        private void btnDelete_Click(object sender,System.EventArgs e) {
            if (lbLesLapins.SelectedIndex == -1) return;
            int position = lbLesLapins.SelectedIndex;
            ((Lapin)lbLesLapins.Items[position]).Remove();
            lbLesLapins.Items[position] = lbLesLapins.Items[position];
        }

        private void btnEdit_Click(object sender,System.EventArgs e) {
            if (lbLesLapins.SelectedIndex == -1) return;
            int position = lbLesLapins.SelectedIndex;
            FeditLapin fEdit = new FeditLapin(State.modified, lbLesLapins.Items, position, this.idCourse);
            fEdit.Show();
        }

        private void btnAdd_Click(object sender,System.EventArgs e) {
            
            FeditLapin fEdit = new FeditLapin(State.added, lbLesLapins.Items, 0, this.idCourse);
            fEdit.Show();
        }

        private void loadCourse(List<Course> listeCourse)
        {
            cbCourse.Items.Clear();
            foreach(Course uneCourse in listeCourse)
            {
                this.cbCourse.Items.Add(uneCourse);
            }
        }

        private void load(List<Lapin> listeLapin)
        {
            lbLesLapins.Items.Clear();
            foreach (Lapin unLapin in listeLapin)
            {
                this.lbLesLapins.Items.Add(unLapin);
            }
            
        }

    }
}
