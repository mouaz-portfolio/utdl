﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;


namespace Utdl.View {
    public partial class FeditLapin:Form {
        private State state;
        private ListBox.ObjectCollection items;
        private int position;
        private int idCourse;

        public FeditLapin(State state, ListBox.ObjectCollection items, int position, int idCourse) {
            InitializeComponent();
      
            this.state = state;
            this.items = items;
            this.position = position;
            this.idCourse = idCourse;
            btnValider.Click += BtnValider_Click1;
            switch (state)
            {
                case State.added:
                    this.Text = "Création d'un lapin";
                    break;
                case State.modified:
                    Lapin lapin = (Lapin)items[position];
                    this.tbId.Text = lapin.Id.ToString();
                    this.tbSurnom.Text = lapin.Surnom;
                    this.tbAge.Text = lapin.Age.ToString();
                    this.tbDossard.Text = lapin.Dossard.ToString();
                    this.tbPosition.Text = lapin.Position.ToString();
                    
                    this.Text = "Modification d'un lapin";
                    break;
                case State.deleted:
                    this.Text = "Suppression d'un lapin";
                    break;
                case State.unChanged:
                    this.Text = "Consultation des lapins";
                    break;
                default:
                    break;
            }
        }

        private void BtnValider_Click1(object sender, EventArgs e)
        {
            if (Verification() == true)
            {
                switch (this.state)
                {
                    case State.added:
                        items.Add(new Lapin(0, tbSurnom.Text, Convert.ToInt32(tbAge.Text), 0, Convert.ToInt32(tbDossard.Text), this.state, this.idCourse));
                        break;
                    case State.modified:
                        Lapin lapin = (Lapin)items[this.position];
                        lapin.Surnom = tbSurnom.Text;
                        lapin.Age = Convert.ToInt32(tbAge.Text);
                        lapin.Dossard = Convert.ToInt32(tbDossard.Text);
                        lapin.State = this.state;
                        items[position] = lapin;
                        break;
                    case State.deleted:
                        break;
                    case State.unChanged:
                        // rien
                        break;
                    default:
                        break;
                }
                this.Close();
            }  
        }

        private bool Verification()
        {
            if (this.tbAge.Text == "" || this.tbDossard.Text == "" || this.tbSurnom.Text == "")
            {
                MessageBox.Show("Il faut remplir les champs !");
                return false;
            }
            else
            {
                foreach (char c in tbAge.Text)
                {
                    if (!char.IsDigit(c))
                    {
                        MessageBox.Show("Error ! Surnom : string, Dossard : int, Age : int");
                        return false;
                    }
                }
                foreach (char c in tbDossard.Text)
                {
                    if (!char.IsDigit(c))
                    {
                        MessageBox.Show("Error ! Surnom : string, Dossard : int, Age : int");
                        return false;
                    }
                }
            }
            return true;
        }


    }
}
