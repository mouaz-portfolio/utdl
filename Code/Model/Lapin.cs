﻿using System;

namespace Model
{
    /// <summary>
    /// Une classe permettant de décrire un lapin
    /// </summary>
    public class Lapin
    {
        private string surnom;
        private int age;
        private int position;
        private int id;
        private State state;
        private int dossard;
        private int idCourse;
        private static Random aleatoire = new Random();

        
        /// <summary>
        /// Une propriété permettant de modifier l'âge du lapin et de l'afficher
        /// </summary>
        public int Age
        {
            set
            {
                this.age = value;
            }
            get
            {
                return this.age;
            }
        }

        public int Id
        {
            get
            {
                return this.id;
            }
        }

        public int Dossard
        {
            get
            {
                return this.dossard;
            }
            set
            {
                this.dossard = value;
            }
        }
        /// <summary>
        /// Accesseur permettant d'afficher la position du lapin
        /// </summary>
        /// <returns></returns>
        public int Position
        {
            get
            {
                return this.position;
            }
            
        }

        public State State
        {
            get
            {
                return this.state;
            }

            set
            {
                this.state = value;
            }
        }

        /// <summary>
        /// Propriété permettant de modifier le nom du lapin et de l'afficher
        /// </summary>
        public string Surnom
        {
            set
            {
                this.surnom = value;
            }
            get
            {
                return this.surnom;
            }
        }

        public int IdCourse
        {
            get
            {
                return this.idCourse;
            }
            set
            {
                this.idCourse = value;
            }
        }

        /// <summary>
        /// Méthode qui permet de faire avancer le lapin (1 à 6 cases)
        /// </summary>
        public void Avancer()
        {
            int val = aleatoire.Next(1, 7);
            this.position += val;
        }

        /// <summary>
        /// Permet de créer un nouveau lapin électronique
        /// </summary>
        /// <param name="id">Quel est l'id du lapin</param>
        /// <param name="surnom">Quel est son nom ?</param>
        /// <param name="age">Quel est son âge ?</param>
        /// <param name="state">Quel est l'étât du lapin</param>
        public Lapin(int id,  string surnom, int age, int position, int dossard, State state, int idCourse)
        {
            this.surnom = surnom;
            this.age = age;
            this.position = position;
            this.id = id;
            this.state = state;
            this.dossard = dossard;
            this.idCourse = idCourse;
        }

        public void Remove()
        {
            this.state = State.deleted;
        }

        /// <summary>
        /// Méthode permettant de retourner l'objet sous la forme d'une châine de caractère
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("Un lapin : {0}, Dossard : {1}, Age : {2}, Position : {3}, State = {4}", this.surnom, this.dossard, this.age, this.position,this.state);
        }



    }
}
