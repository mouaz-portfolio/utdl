﻿using System;
using System.Collections.Generic;

namespace Model
{
    /// <summary>
    /// Une classe permettant de la course
    /// </summary>
    public class Course
    {
        private int id;
        private int distance;
        private State state;
        private List<Lapin> participer = new List<Lapin>();

        /// <summary>
        /// Constructeur qui permet de créer une course
        /// </summary>
        /// <param name="distance">Distance de la course</param>
        public Course(int id, int distance, State state)
        {
            this.distance = distance;
            this.id = id;
            this.state = state;
        }

        /// <summary>
        /// Méthode qui permet d'ajouter un participants
        /// </summary>
        /// <param name="nouveauParticipant">nouveauParticipant est de type Lapin et désigne le nouveau lapin a ajouté</param>
        public void Add(Lapin nouveauParticipant)
        {
            this.participer.Add(nouveauParticipant);
        }


        /// <summary>
        /// Méthode qui permet de démarrer la course et de faire avancer les lapins de x pas. X désigne la distance de la course
        /// </summary>
        public void Départ()
        {
            for (int i = 1; i < this.distance; i++)
            {
                foreach (Lapin lap in this.participer)
                {
                    lap.Avancer();
                }

            }
        }

        /// <summary>
        /// Méthode qui permet de désigné le gagnant de la course
        /// </summary>
        /// <returns>Retourne un type Lapin</returns>
        public Lapin Gagnant()
        {
            int position = 0;
            Lapin winner= null;
            foreach (Lapin lap in this.participer)
            {
                
                if(lap.Position > position)
                {
                    winner = lap;
                }
                position = lap.Position;

            }
            return winner;
        }

        /// <summary>
        /// Accesseur qui permet de retourner la liste de lapins
        /// </summary>
        /// <returns>Retourne une liste de lapins</returns>
        public List<Lapin> GetParticiper()
        {
            return this.participer;
        }

        /// <summary>
        /// Méthode qui permet de supprimer un lapin de la liste
        /// </summary>
        /// <param name="position">Position désigne l'indice du lapin dans la collection</param>
        public void RemoveAt(int position)
        {
            if(position >=0 && position < this.participer.Count)
            {
                this.participer.RemoveAt(position);
            }
            
        }

        /// <summary>
        /// Méthode qui permet d'afficher la longueur de la collection de lapin
        /// </summary>
        public int Count
        {
            get
            {
                return this.participer.Count;
            }
           
        }

        public int Distance
        {
            get
            {
                return this.distance;
            }
            set
            {
                this.distance = value;
            }
        }

        public int Id
        {
            get
            {
                return this.id;
            }
        }

        /// <summary>
        /// Propriété qui permet de retourner la liste de lapins
        /// </summary>
        public List<Lapin> Participer
        {
            get
            {
                return this.participer;
            }
        } 

        public State State
        {
            get
            {
                return this.state;
            }

            set
            {
                this.state = value;
            }
        }

        /// <summary>
        /// Indexeur qui permet d'afficher un lapin de la collection en particulier
        /// </summary>
        /// <param name="position">Position désigne l'indice où se trouve le lapin dans la collection</param>
        /// <returns>Retourne un type Lapin</returns>
        public Lapin IemeLapin(int position)
        {
            return this.participer[position];
        
        }
        
        /// <summary>
        /// Methode qui permet de désigner les 3 meilleurs lapins de la course et les mettre dans une nouvelle liste "podium".
        /// </summary>
        /// <returns>Retorune une liste de lapin</returns>
        public List<Lapin> Podium()
        {
            List<Lapin> liste = this.participer;
            List<Lapin> podium = new List<Lapin>();

            for (int i = 0; i < 3; i++)
            {
                Lapin winner = LeGagnant(liste);
                
                liste.Remove(winner);
                podium.Add(winner);
            }
    
            return podium;
        }

        /// <summary>
        /// Méthode qui permet de désigné le gagnant de la course d'une liste en particulier
        /// </summary>
        /// <param name="l">l désigne le nom d'une liste de lapin</param>
        /// <returns>Retourne un type Lapin</returns>
        public Lapin LeGagnant(List<Lapin> l)
        {
            int position = 0;
            Lapin winner = null;
            foreach (Lapin lap in l)
            {
                if (lap.Position > position)
                {
                    winner = lap;
                }
                position = lap.Position;

            }
            return winner;
        }

        public void Remove()
        {
            this.state = State.deleted;
        }

        public override string ToString()
        {
            return String.Format("Course n°{0} : Distance = {1} , State = {2}",this.id,this.distance,this.state);
        }
    }
}
