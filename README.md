# UltraTrailDesLapins ~ Mouaz MOHAMED

UltraTrailDesLapins ou UTDL est une application de bureau Windows développée en C# via le framework Microsoft .NET et l'interface graphique Windows Forms permettant d'organiser des courses de lapins.

---

## Diagramme des cas d'utilisation (User Case) de mon application
```plantuml
left to right direction
:Organisateur: as Organisateur
package <uc>UltraTrail {
    Organisateur --- (Ajouter une course)
    Organisateur --- (Modifier une course)
    Organisateur --- (Supprimer une course)
    Organisateur --- (Inscrire des lapins à une course)
    (Inscrire des lapins à une course) ..> (Sélectionner une course existante) : <<include>>
    (Inscrire des lapins à une course) <.. (Ajouter le lapin dans une course existante) : <<extends>>
    (Inscrire des lapins à une course) <.. (Modifier l'inscription du lapin dans une course existante) : <<extends>>
    (Inscrire des lapins à une course) <.. (Supprimer l'inscription du lapin dans une course existante) : <<extends>>
}

note top of (Inscrire des lapins à une course)
  1. L'organisateur sélectionne une course
  2. La liste des lapins participants est affichée
  
  <u>Scénarios inclus :</u>
  - L'organisateur peut ajouter un lapin
  - L'organisateur peut modifier un lapin
  - L'organisateur peut supprimer un lapin
  
  3. L'organisateur valide ses modifications
end note

@enduml
```
<br></br>

---

### Diagramme de classes
Voici les diagrammes de classes de mon application :<br>
![mcd_dao](Annexes/mcd_dao.png)<br><br>
![mcd_model](Annexes/mcd_model.png)<br><br>

La mise en place des différentes classes :<br>
- model : Représente les classes métiers, la logique métier de la programmation
- dao : Permet d'organiser la persistance des données dans une base de données.
- view : Les vues représentent l'interface utilisateur de l'application.
<br></br>

---

## Accueil de mon application
Voici l'accueil de mon application où nous pouvons gérer les courses et les lapins participants à ces courses :<br>
![accueil](Annexes/accueil.png)<br><br>

---

## Gérer les courses
Voici l'endroit de mon application où nous pouvons gérer les courses (ajouter, modifier et supprimer des courses) :<br>
![courses1](Annexes/courses1.png)<br><br>
![courses2](Annexes/courses2.png)<br><br>
![courses3](Annexes/courses3.png)<br><br>

---

## Gérer les lapins
Voici l'endroit de mon application où nous pouvons gérer les lapins qui participent aux courses déjà existantes (ajouter, modifier et supprimer les lapins qui participent à une course déjà existante) :<br>
![course1](Annexes/course1.png)<br><br>
![course2](Annexes/course2.png)<br><br>
![course3](Annexes/course3.png)<br><br>